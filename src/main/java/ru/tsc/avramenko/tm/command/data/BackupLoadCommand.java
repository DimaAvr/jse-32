package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.Domain;
import ru.tsc.avramenko.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.concurrent.Executors;

public class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public final static String BACKUP_LOAD = "backup-load";

    @NotNull
    @Override
    public String name() {
        return BACKUP_LOAD;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Nullable
    public Role[] roles() {
        if (!serviceLocator.getAuthService().isAuth()) {
            return null;
        }
        else {
            return new Role[]{Role.ADMIN};
        }
    }

}